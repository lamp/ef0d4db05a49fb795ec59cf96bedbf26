function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function drawPixel(x, y, color) {
  MPP.addons.draw.mkline(x-1,y,x,y,10,color)
}

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var img = document.createElement("img");
img.crossOrigin = "Anonymous";
img.addEventListener('load', function(){
    console.log(img);
    var canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;
    var c = canvas.getContext('2d');
    c.drawImage(img, 0, 0, img.width, img.height);
    console.log(canvas);
    
    var pos1 = [128-img.width/2,128-img.height/2], pos2 = [128+img.width/2,128+img.height/2];
    //var pos1 = [0,0], pos2 = [64,64];
    
    var ii=0;
    for (let x = pos1[0], xo = 0; x < pos2[0]; x++, xo++) 
    for (let y = pos1[1], yo = 0; y < pos2[1]; y++, yo++) {
      setTimeout(()=>{
          var rgb = c.getImageData(xo,yo, 1, 1).data;
          drawPixel(x,y, rgbToHex(rgb[0], rgb[1], rgb[2]));
      } ,++ii * 4);
    }
});
img.src = "https://mpp.terrium.net/meow64.png";
